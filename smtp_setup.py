#!/usr/bin/env python
# -*- coding: utf-8 -*-

import getpass
import fileinput,re
import os
import sys

def create_dir():
  if not os.path.exists('config'):
    os.makedirs('config')

def create_file():
  if not os.path.exists('config/smtp_config.txt'):
    with open('config/smtp_config.txt', 'w'): pass
    print(smtp_filepath + " created!")

def write_parameter( parameter, smtp_filepath ):
  print("\n")
  print("Setting up SMTP parameters...")
  smtp_host = input("Enter SMTP hostname: ")
  smtp_user_login = input("Enter SMTP user login, ex. user@" + smtp_host +"? ")
  smtp_user_password = getpass.getpass("SMTP user password? ")
  with open(smtp_filepath, "w") as text_file:
    print("smtp_host: {}".format(smtp_host), file=text_file)
    print("smtp_user_login: {}".format(smtp_user_login), file=text_file)
    print("smtp_user_password: {}".format(smtp_user_password), file=text_file)

# Returns the parameter from the specified file
def get_parameter( parameter, smtp_filepath ):
    # Check if file exists
    if not os.path.isfile(smtp_filepath):
        print("File %s not found, creating it."%smtp_filepath)
        create_dir()
        create_file()
        write_parameter( parameter, smtp_filepath )
        print("\n")
        print("SMTP setup done!\n")

    # Find parameter in file
    with open( smtp_filepath ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + " Missing parameter %s "%parameter)
    sys.exit(0)

# Load secrets from secrets file
smtp_filepath = "config/smtp_config.txt"
smtp_host = get_parameter("smtp_host", smtp_filepath)
smtp_user_login = get_parameter("smtp_user_login", smtp_filepath)
smtp_user_pass = get_parameter("smtp_user_pass", smtp_filepath)

